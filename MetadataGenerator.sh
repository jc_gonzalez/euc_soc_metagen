#!/bin/bash 
#------------------------------------------------------------------------------------
# MetadataGenerator.sh
# Metadata (XML) File Generator
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYTHONPATH=${SCRIPTPATH}

${PYTHON:=python3} ${SCRIPTPATH}/metagen/metagen.py $*

# Clean up
rm -rf ${TMPDIR}
