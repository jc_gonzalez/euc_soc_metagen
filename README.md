Euclid Product Metadata Generator
==========================================

This directory and all its sub-directories contain the launcher and modules of the
Euclid Metadata Generator.

This tool takes an SOC-related produt as input file, and tries to extract enough 
information to generate the associated XML metadata file, used to ingest the
product into the Euclid EAS.

Installation and Execution
--------------------------

For installing and running this tool, just clone the `git` repository, and launch the script 
`MetadataGenerator.sh` to get a message on the valid command line arguments.

License
--------

You should have received a copy of the GNU Lesser General Public License
along with QPF (please, see [COPYING][1] and [COPYING.LESSER][2] for
information on this license).  If not, see <http://www.gnu.org/licenses/>.

[1]: ./COPYING
[2]: ./COPYING.LESSER
