Change Log
====================

All notable changes to the Euclid Metadata Generator software project will be documented 
in this file.

V1.0 / 2020-06-09
--------------------------

First release of the Euclid Metadata Generator.

