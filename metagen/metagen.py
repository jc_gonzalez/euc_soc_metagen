#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generation of Metadata Files

This tool takes an SOC-related produt as input file, and tries to extract enough
information to generate the associated XML metadata file, used to ingest the
product into the Euclid EAS.

"""
#----------------------------------------------------------------------

import os
import sys
import argparse

from metadata_generator import InputType, MetadataGenerator

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

def configureLogs():
    logger.setLevel(logging.DEBUG)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    for lname in os.getenv('LOGGING_MODULES','').split(':'):
        lgr = logging.getLogger(lname)
        if not lgr.handlers: lgr.addHandler(c_handler)

def getArgs():
    '''
    Parse arguments from command line

    :return: args structure
    '''
    parser = argparse.ArgumentParser(description='Euclid SOC-related Products Metadata File Generator',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('files', metavar='FILE', type=str, nargs='+',
                        help='Input file name(s)')
    parser.add_argument('-t', '--type', dest='type', type=str, default="QLA",
                        help='Input file type ({})'.format(', '.join([f'{n}'
                                                                      for n,v in InputType.__members__.items()])))
    parser.add_argument('-o', '--output', dest='output_file', type=str,
                        help='Output file name (full path)')
    return parser.parse_args()

def greetings():
    """
    Says hello
    """
    logger.info('='*60)
    logger.info('metagen - Generate XML Metadata files for several SOC-related products')
    logger.info('='*60)


def main():
    """
    Main program
    """
    configureLogs()

    args = getArgs()
    fileType = InputType.__members__[args.type]

    greetings()

    md = MetadataGenerator(args.files, fileType, args.output_file)
    xmlFile = md.run()

    logger.info(f'Metadata Object File created: {xmlFile}')


if __name__ == '__main__':
    main()
