#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Metadata Generator

This module implements a class that uses the provided input files to generate their associated
metadata object files.
"""
#----------------------------------------------------------------------

import os
import sys
import zipfile

from enum import Enum, auto as autoEnum
from metadata_templates import RawScienceTpl, QlaReportTpl, OpPlanProdsTpl, LE1EnhancedTpl, \
                               MIBTpl, ICRTpl, OEMTpl, \
                               metaHeader, metaListOfFiles

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

class InputType(Enum):
    """
    Type of the input file(s) provided for metadata object generation
    """
    SCI = autoEnum()
    QLA = autoEnum()
    ICR = autoEnum()
    OP_PLAN = autoEnum()
    LE1_VIS = autoEnum()
    LE1_NISP = autoEnum()
    MIB = autoEnum()
    OEM = autoEnum()

MetaTemplate = { 'SCI': RawScienceTpl,
                 'QLA': QlaReportTpl,
                 'ICR': ICRTpl,
                 'OP_PLAN': OpPlanProdsTpl,
                 'LE1_VIS': LE1EnhancedTpl,
                 'LE1_NISP': LE1EnhancedTpl,
                 'MIB': MIBTpl,
                 'OEM': OEMTpl }

ProductType = { 'SCI': 'DpdRawScience',
                'QLA': 'DpdQlaReport',
                'ICR': 'DpdInstrCmdRqst',
                'OP_PLAN': 'DpdOpPlaProducts',
                'LE1_VIS': 'DpdVisRawFrame',
                'LE1_NISP': 'DpdNispRawFrame',
                'MIB': 'DpdMIB',
                'OEM': 'DpdOrbEphemMsg' }

def createZip(output_dir, file_list):
    """
    Creates the final zip file with all the generated files in the output directory
    """
    baseName = os.path.basename(file_list[-1])
    zipName = os.path.join(output_dir, '.'.join([os.path.splitext(baseName)[0], 'zip']))
    with zipfile.ZipFile(zipName, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for file in file_list:
            zipf.write(file)
    return zipName


class MetadataGenerator:
    """
    Master class to process the provided input files and generate their associated metadata object
    """
    def __init__(self, files, file_type, output_file):
        self.files = files
        self.type = file_type
        self.ofile = output_file

    def process_le1enh(self):
        """
        Process LE1 Enhanced file, to update the already existing metadata object
        """
        return None

    def run(self):
        """
        Method to launch the processing of the files and the generation of metadata
        """
        if self.type in [InputType.LE1_VIS, InputType.LE1_NISP]:
            return self.process_le1enh()

        tpl = MetaTemplate[self.type.name]
        if self.ofile:
            xmlFile = self.ofile
            id, _ = os.path.splitext(os.path.basename(xmlFile))
        else:
            mainFile = self.files[0]
            id, _ = os.path.splitext(os.path.basename(mainFile))
            xmlFile = os.path.join(os.path.dirname(os.path.realpath(mainFile)), '.'.join([id, 'xml']))
        xmlObj1 = tpl.replace('{Header}', metaHeader(id, ProductType[self.type.name]))
        xmlObj = xmlObj1.replace('{FileNames}', metaListOfFiles(self.files))

        with open(xmlFile, 'w') as fxml:
            fxml.write(xmlObj)

        return xmlFile


if __name__ == '__main__':
    pass
