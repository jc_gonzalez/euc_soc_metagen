#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Metadata Templates

Thi module contains several string constants that are the templates of the different XML
metadata objects
"""
#----------------------------------------------------------------------

import os
import sys

from datetime import datetime

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

import logging

logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

HeaderTpl = """    <Header>
        <ProductId>{0}</ProductId>
        <ProductType>{1}</ProductType>
        <SoftwareName>{2}</SoftwareName>
        <SoftwareRelease>{3}</SoftwareRelease>
        <ProdSDC>SOC</ProdSDC>
        <DataSetRelease>{4}</DataSetRelease>
        <Purpose>{5}</Purpose>
        <PlanId>N/A</PlanId>
        <PPOId>N/A</PPOId>
        <PipelineDefinitionId>N/A</PipelineDefinitionId>
        <PipelineRun>N/A</PipelineRun>
        <ExitStatusCode>0</ExitStatusCode>
        <ManualValidationStatus>UNKNOWN</ManualValidationStatus>
        <Curator>J.C.Gonzalez</Curator>
        <CreationDate>{6}</CreationDate>
    </Header>"""

FileNameTpl = "        <FileName>{0}</FileName>"

RawScienceTpl = """<?xml version="1.0" encoding="UTF-8"?>
<raw:DpdRawScience xmlns:sys="http://euclid.esa.org/schema/sys"
 xmlns:soc="http://euclid.esa.org/schema/pro/soc"
 xmlns:dqc="http://euclid.esa.org/schema/bas/dqc"
 xmlns:ppr="http://euclid.esa.org/schema/bas/ppr"
 xmlns:dss="http://euclid.esa.org/schema/sys/dss"
 xmlns:raw="http://euclid.esa.org/schema/dpd/soc/raw"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://euclid.esa.org/schema/dpd/soc/raw file:/ST_DataModel/ST_DM_Schema/auxdir/ST_DM_Schema/dpd/soc/euc-test-soc-RawScience.xsd">
{Header}
    <Data filestatus="PROPOSED">
{FileNames}
    </Data>
</raw:DpdRawScience>"""

QlaReportTpl = """<?xml version="1.0" encoding="UTF-8"?>
<qla:DpdQlaReport xmlns:dss="http://euclid.esa.org/schema/sys/dss"
 xmlns:sys="http://euclid.esa.org/schema/sys"
 xmlns:soc="http://euclid.esa.org/schema/pro/soc"
 xmlns:dqc="http://euclid.esa.org/schema/bas/dqc"
 xmlns:ppr="http://euclid.esa.org/schema/bas/ppr"
 xmlns:qla="http://euclid.esa.org/schema/dpd/soc/qla"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://euclid.esa.org/schema/dpd/soc/qla file:/ST_DataModel/ST_DM_Schema/auxdir/ST_DM_Schema/dpd/soc/euc-test-soc-QlaReport.xsd">
{Header}
    <Data filestatus="PROPOSED">
{FileNames}
    </Data>
</qla:DpdQlaReport>
"""

OpPlanProdsTpl = """<?xml version="1.0" encoding="UTF-8"?>
<opla:DpdOpPlaProducts xmlns:soc="http://euclid.esa.org/schema/pro/soc"
 xmlns:sys="http://euclid.esa.org/schema/sys"
 xmlns:dqc="http://euclid.esa.org/schema/bas/dqc"
 xmlns:ppr="http://euclid.esa.org/schema/bas/ppr"
 xmlns:dss="http://euclid.esa.org/schema/sys/dss"
 xmlns:opla="http://euclid.esa.org/schema/dpd/soc/opla"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://euclid.esa.org/schema/dpd/soc/opla file:/Users/jcgonzalez/ws/ECDM/ST_DataModel/ST_DM_Schema/auxdir/ST_DM_Schema/dpd/soc/euc-test-soc-OpPlaProducts.xsd">
{Header}
    <Data filestatus="PROPOSED">
{FileNames}
    </Data>
</opla:DpdOpPlaProducts>
"""

MIBTpl = """<?xml version="1.0" encoding="UTF-8"?>
<mib:DpdMIB xmlns:sys="http://euclid.esa.org/schema/sys"
 xmlns:soc="http://euclid.esa.org/schema/pro/soc"
 xmlns:dqc="http://euclid.esa.org/schema/bas/dqc"
 xmlns:ppr="http://euclid.esa.org/schema/bas/ppr"
 xmlns:dss="http://euclid.esa.org/schema/sys/dss"
 xmlns:mib="http://euclid.esa.org/schema/dpd/soc/mib"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://euclid.esa.org/schema/dpd/soc/mib file:/ST_DataModel/ST_DM_Schema/auxdir/ST_DM_Schema/dpd/soc/euc-test-soc-MIB.xsd">
{Header}
    <Data filestatus="PROPOSED">
{FileNames}
    </Data>
</raw:DpdMIB>"""

ICRTpl = """<?xml version="1.0" encoding="UTF-8"?>
<mib:DpdInstrCmdRqst xmlns:sys="http://euclid.esa.org/schema/sys"
 xmlns:soc="http://euclid.esa.org/schema/pro/soc"
 xmlns:dqc="http://euclid.esa.org/schema/bas/dqc"
 xmlns:ppr="http://euclid.esa.org/schema/bas/ppr"
 xmlns:dss="http://euclid.esa.org/schema/sys/dss"
 xmlns:mib="http://euclid.esa.org/schema/dpd/soc/icr"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://euclid.esa.org/schema/dpd/soc/icr file:/ST_DataModel/ST_DM_Schema/auxdir/ST_DM_Schema/dpd/soc/euc-test-soc-InstrCmdRqst.xsd">
{Header}
    <Data filestatus="PROPOSED">
{FileNames}
    </Data>
</raw:DpdInstrCmdRqst>"""

OEMTpl = """<?xml version="1.0" encoding="UTF-8"?>
<mib:DpdOrbEphemMsg xmlns:sys="http://euclid.esa.org/schema/sys"
 xmlns:soc="http://euclid.esa.org/schema/pro/soc"
 xmlns:dqc="http://euclid.esa.org/schema/bas/dqc"
 xmlns:ppr="http://euclid.esa.org/schema/bas/ppr"
 xmlns:dss="http://euclid.esa.org/schema/sys/dss"
 xmlns:mib="http://euclid.esa.org/schema/dpd/soc/oem"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://euclid.esa.org/schema/dpd/soc/oem file:/ST_DataModel/ST_DM_Schema/auxdir/ST_DM_Schema/dpd/soc/euc-test-soc-OrbEphemMsg.xsd">
{Header}
    <Data filestatus="PROPOSED">
{FileNames}
    </Data>
</raw:DpdOrbEphemMsg>"""

LE1EnhancedTpl = ""

def metaHeader(id, type, swName='SOC', swVersion='3.1.0', datasetRelease='', purpose='TEST', created=None):
    """
    Returns the Header component of the Metadata XML Object, with the variable fields filled
    """
    if not created: created = datetime.now().strftime("%Y%m%dT%H%M%S.0Z")
    return HeaderTpl.format(id, type, swName, swVersion, datasetRelease, purpose, created)


def metaListOfFiles(files):
    """
    Returns a set of lines with file descriptions for the Data component in the metadata object
    """
    if len(files) < 1: return ''
    return '\n'.join([FileNameTpl.format(os.path.basename(file)) for file in files])


if __name__ == '__main__':
    pass
